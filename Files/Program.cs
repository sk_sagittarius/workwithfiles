﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Files
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] str;
            using (FileStream stream = File.OpenRead("input.txt"))
            {
                byte[] bytesData = new byte[stream.Length];
                stream.Read(bytesData, 0, bytesData.Length);

                string result = Encoding.UTF8.GetString(bytesData);
                Console.WriteLine(result);
                str = result.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            }
            //Console.WriteLine(str[0] + "\n" + str[1]);
            int res = int.Parse(str[0]) + int.Parse(str[1]);
            //Console.WriteLine(res);
            //Console.ReadLine();

            string data = res.ToString();
            using (FileStream stream = new FileStream("output.txt", FileMode.Create))
            {
                byte[] bytesData = Encoding.UTF8.GetBytes(data);
                stream.Write(bytesData, 0, bytesData.Length);
            }
        }
    }
}
